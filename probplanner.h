/* -*-C++-*- */
/*
 * Probalistic Planner.
 *
 * Copyright 2011 Moisés Martínez Muñoz Universidad Carlos III Madrid
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _PROBPLANNER_H
#define _PROBPLANNER_H

class ProbPlanner : public Planner
{
public:
  ProbPlanner(const Problem& problem) : Planner(problem) {}
  ProbPlanner(std::string domain_file, const Problem& problem) : Planner(problem) 
  { 
  	domain_file_name_ = domain_file;
	observations_file_name_ = str_arg_observations_file_name;
	egreedyness=1;
  }

  virtual void initRound();
  virtual ~ProbPlanner() {}
  virtual const Action* decideAction(const AtomSet& state_atoms,const ValueMap& state_values);
  virtual void endRound();

private:  

  std::string domain_file_name_;
  std::string observations_file_name_;
  AtomSet static_state_atoms_;
  AtomSet previous_state_atoms_;
  ValueMap previous_state_values_;
  std::vector<std::string> plan_;
  int index_current_action_;

  double egreedyness;

  bool plan(std::string domain_file_name, const AtomSet& state_atoms,const ValueMap& state_values);
  bool plan_ff(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values);
  
  bool replan(const AtomSet& state_atoms,const ValueMap& state_values);
  bool repairplan();

  int generate_reproblem(std::string domain_file, std::string reproblem_domain_file, const AtomSet& state_atoms,const ValueMap& state_values);
  int print_pending_plan();
  int write_Observation(std::string observations_file_name, std::string action, const AtomSet& state_atoms,const ValueMap& state_values, std::string result);

  AtomSet get_previous_state_atoms();
  ValueMap get_previous_state_values();

  void set_previous_state(const AtomSet& state_atoms,const ValueMap& state_values);
};

#end_if
