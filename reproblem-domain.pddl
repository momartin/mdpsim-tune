(define (domain Rover)
(:requirements :typing :fluents)
(:types rover waypoint store camera mode lander objective)

(:predicates (at ?x - rover ?y - waypoint) 
             (at_lander ?x - lander ?y - waypoint)
             (can_traverse ?r - rover ?x - waypoint ?y - waypoint)
	     (equipped_for_soil_analysis ?r - rover)
             (equipped_for_rock_analysis ?r - rover)
             (equipped_for_imaging ?r - rover)
             (empty ?s - store)
             (have_rock_analysis ?r - rover ?w - waypoint)
             (have_soil_analysis ?r - rover ?w - waypoint)
             (full ?s - store)
	     (calibrated ?c - camera ?r - rover) 
	     (supports ?c - camera ?m - mode)
             (available ?r - rover)
             (visible ?w - waypoint ?p - waypoint)
             (have_image ?r - rover ?o - objective ?m - mode)
             (communicated_soil_data ?w - waypoint)
             (communicated_rock_data ?w - waypoint)
             (communicated_image_data ?o - objective ?m - mode)
	     (at_soil_sample ?w - waypoint)
	     (at_rock_sample ?w - waypoint)
             (visible_from ?o - objective ?w - waypoint)
	     (store_of ?s - store ?r - rover)
	     (calibration_target ?i - camera ?o - objective)
	     (on_board ?i - camera ?r - rover)
	     (channel_free ?l - lander)
	     (in_sun ?w - waypoint)

)

(:functions (energy ?r - rover) (recharges) (actions))
	
(:action navigate
:parameters (?x - rover ?y - waypoint ?z - waypoint) 
:precondition (and (can_traverse ?x ?y ?z) (available ?x) (at ?x ?y) 
                (visible ?y ?z) (>= (energy ?x) 8)
	    )
:effect (and (decrease (energy ?x) 8) (not (at ?x ?y)) (at ?x ?z)
		)
)

(:action recharge
:parameters (?x - rover ?w - waypoint)
:precondition (and (at ?x ?w) (in_sun ?w) (<= (energy ?x) 80))
:effect (and (increase (energy ?x) 20) (increase (recharges) 1) (increase (actions) 1)) 
)

(:action sample_soil
:parameters (?x - rover ?s - store ?p - waypoint)
:precondition (and (at ?x ?p)(>= (energy ?x) 3) (at_soil_sample ?p) (equipped_for_soil_analysis ?x) (store_of ?s ?x) (empty ?s)
		)
:effect (and (not (empty ?s)) (full ?s) (decrease (energy ?x) 3) (have_soil_analysis ?x ?p) (not (at_soil_sample ?p)) (increase (actions) 1)
		)
)

(:action sample_rock
:parameters (?x - rover ?s - store ?p - waypoint)
:precondition (and  (at ?x ?p) (>= (energy ?x) 5)(at_rock_sample ?p) (equipped_for_rock_analysis ?x) (store_of ?s ?x)(empty ?s)
		)
:effect (and (not (empty ?s)) (full ?s) (decrease (energy ?x) 5) (have_rock_analysis ?x ?p) (not (at_rock_sample ?p)) (increase (actions) 1)
		)
)

(:action drop
:parameters (?x - rover ?y - store)
:precondition (and (store_of ?y ?x) (full ?y)
		)
:effect (and (not (full ?y)) (empty ?y) (increase (actions) 1)
	)
)

(:action calibrate
 :parameters (?r - rover ?i - camera ?t - objective ?w - waypoint)
 :precondition (and (equipped_for_imaging ?r) (>= (energy ?r) 2)(calibration_target ?i ?t) (at ?r ?w) (visible_from ?t ?w)(on_board ?i ?r)
		)
 :effect (and (decrease (energy ?r) 2)(calibrated ?i ?r) (increase (actions) 1))
)




(:action take_image
 :parameters (?r - rover ?p - waypoint ?o - objective ?i - camera ?m - mode)
 :precondition (and (calibrated ?i ?r)
			 (on_board ?i ?r)
                      (equipped_for_imaging ?r)
                      (supports ?i ?m)
			  (visible_from ?o ?p)
                     (at ?r ?p)
			(>= (energy ?r) 1)
               )
 :effect (and (have_image ?r ?o ?m)(not (calibrated ?i ?r))(decrease (energy ?r) 1) (increase (actions) 1)
		)
)

(:action communicate_soil_data
 :parameters (?r - rover ?l - lander ?p - waypoint ?x - waypoint ?y - waypoint)
 :precondition (and (at ?r ?x)(at_lander ?l ?y)(have_soil_analysis ?r ?p) 
                   (visible ?x ?y)(available ?r)(channel_free ?l)(>= (energy ?r) 4)
            )
 :effect (and (not (available ?r))(not (channel_free ?l))
(channel_free ?l) (communicated_soil_data ?p)(available ?r)(decrease (energy ?r) 4) (increase (actions) 1)
	)
)

(:action communicate_rock_data
 :parameters (?r - rover ?l - lander ?p - waypoint ?x - waypoint ?y - waypoint)
 :precondition (and (at ?r ?x)(at_lander ?l ?y)(have_rock_analysis ?r ?p)(>= (energy ?r) 4)
                   (visible ?x ?y)(available ?r)(channel_free ?l)
            )
 :effect (and   (not (available ?r))(not (channel_free ?l))
(channel_free ?l)
(communicated_rock_data ?p)(available ?r)(decrease (energy ?r) 4) (increase (actions) 1)
          )
)


(:action communicate_image_data
 :parameters (?r - rover ?l - lander ?o - objective ?m - mode ?x - waypoint ?y - waypoint)
 :precondition (and (at ?r ?x)(at_lander ?l ?y)(have_image ?r ?o ?m)(visible ?x ?y)(available ?r)(channel_free ?l)(>= (energy ?r) 6)
            )
 :effect (and (not (available ?r))(not (channel_free ?l))
(channel_free ?l)
(communicated_image_data ?o ?m)(available ?r)(decrease (energy ?r) 6) (increase (actions) 1)
          )
)
)
(CALIBRATE ROVER0 CAMERA0 OBJECTIVE1 WAYPOINT3)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_soil_sample waypoint2) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (at_rock_sample waypoint3) (channel_free general) (at rover0 waypoint3) (available rover0) (empty rover0store)|
(TAKE_IMAGE ROVER0 WAYPOINT3 OBJECTIVE1 CAMERA0 HIGH_RES)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_soil_sample waypoint2) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (at_rock_sample waypoint3) (channel_free general) (at rover0 waypoint3) (available rover0) (empty rover0store) (calibrated camera0 rover0)|
(COMMUNICATE_IMAGE_DATA ROVER0 GENERAL OBJECTIVE1 HIGH_RES WAYPOINT3 WAYPOINT0)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_soil_sample waypoint2) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (at_rock_sample waypoint3) (channel_free general) (at rover0 waypoint3) (available rover0) (empty rover0store) (have_image rover0 objective1 high_res)|
(SAMPLE_ROCK ROVER0 ROVER0STORE WAYPOINT3)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_soil_sample waypoint2) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (at_rock_sample waypoint3) (channel_free general) (at rover0 waypoint3) (available rover0) (empty rover0store) (communicated_image_data objective1 high_res) (have_image rover0 objective1 high_res)|
(NAVIGATE ROVER0 WAYPOINT3 WAYPOINT1)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_soil_sample waypoint2) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (channel_free general) (at rover0 waypoint3) (available rover0) (communicated_image_data objective1 high_res) (full rover0store) (have_image rover0 objective1 high_res) (have_rock_analysis rover0 waypoint3)|
(NAVIGATE ROVER0 WAYPOINT1 WAYPOINT2)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_soil_sample waypoint2) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (channel_free general) (available rover0) (communicated_image_data objective1 high_res) (full rover0store) (at rover0 waypoint1) (have_image rover0 objective1 high_res) (have_rock_analysis rover0 waypoint3)|
(DROP ROVER0 ROVER0STORE)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_soil_sample waypoint2) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (channel_free general) (available rover0) (communicated_image_data objective1 high_res) (full rover0store) (at rover0 waypoint2) (have_image rover0 objective1 high_res) (have_rock_analysis rover0 waypoint3)|
(SAMPLE_SOIL ROVER0 ROVER0STORE WAYPOINT2)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_soil_sample waypoint2) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (channel_free general) (available rover0) (empty rover0store) (communicated_image_data objective1 high_res) (at rover0 waypoint2) (have_image rover0 objective1 high_res) (have_rock_analysis rover0 waypoint3)|
(COMMUNICATE_SOIL_DATA ROVER0 GENERAL WAYPOINT2 WAYPOINT2 WAYPOINT0)|success| (visible waypoint1 waypoint0) (visible waypoint0 waypoint1) (visible waypoint2 waypoint0) (visible waypoint0 waypoint2) (visible waypoint2 waypoint1) (visible waypoint1 waypoint2) (visible waypoint3 waypoint0) (visible waypoint0 waypoint3) (visible waypoint3 waypoint1) (visible waypoint1 waypoint3) (visible waypoint3 waypoint2) (visible waypoint2 waypoint3) (in_sun waypoint0) (at_lander general waypoint0) (store_of rover0store rover0) (equipped_for_soil_analysis rover0) (equipped_for_rock_analysis rover0) (equipped_for_imaging rover0) (can_traverse rover0 waypoint3 waypoint0) (can_traverse rover0 waypoint0 waypoint3) (can_traverse rover0 waypoint3 waypoint1) (can_traverse rover0 waypoint1 waypoint3) (can_traverse rover0 waypoint1 waypoint2) (can_traverse rover0 waypoint2 waypoint1) (on_board camera0 rover0) (calibration_target camera0 objective1) (supports camera0 colour) (supports camera0 high_res) (visible_from objective0 waypoint0) (visible_from objective0 waypoint1) (visible_from objective0 waypoint2) (visible_from objective0 waypoint3) (visible_from objective1 waypoint0) (visible_from objective1 waypoint1) (visible_from objective1 waypoint2) (visible_from objective1 waypoint3) (at_soil_sample waypoint0) (at_rock_sample waypoint1) (at_rock_sample waypoint2) (at_soil_sample waypoint3) (channel_free general) (available rover0) (communicated_image_data objective1 high_res) (full rover0store) (at rover0 waypoint2) (have_image rover0 objective1 high_res) (have_rock_analysis rover0 waypoint3) (have_soil_analysis rover0 waypoint2)|


(define (problem roverprob1234)
    (:domain rover)
    (:objects
  camera0 - camera
  colour - mode
  general - lander
  high_res - mode
  low_res - mode
  objective0 - objective
  objective1 - objective
  rover0 - rover
  rover0store - store
  waypoint0 - waypoint
  waypoint1 - waypoint
  waypoint2 - waypoint
  waypoint3 - waypoint) 
    (:init 
  (visible waypoint1 waypoint0)
  (visible waypoint0 waypoint1)
  (visible waypoint2 waypoint0)
  (visible waypoint0 waypoint2)
  (visible waypoint2 waypoint1)
  (visible waypoint1 waypoint2)
  (visible waypoint3 waypoint0)
  (visible waypoint0 waypoint3)
  (visible waypoint3 waypoint1)
  (visible waypoint1 waypoint3)
  (visible waypoint3 waypoint2)
  (visible waypoint2 waypoint3)
  (in_sun waypoint0)
  (at_lander general waypoint0)
  (store_of rover0store rover0)
  (equipped_for_soil_analysis rover0)
  (equipped_for_rock_analysis rover0)
  (equipped_for_imaging rover0)
  (can_traverse rover0 waypoint3 waypoint0)
  (can_traverse rover0 waypoint0 waypoint3)
  (can_traverse rover0 waypoint3 waypoint1)
  (can_traverse rover0 waypoint1 waypoint3)
  (can_traverse rover0 waypoint1 waypoint2)
  (can_traverse rover0 waypoint2 waypoint1)
  (on_board camera0 rover0)
  (calibration_target camera0 objective1)
  (supports camera0 colour)
  (supports camera0 high_res)
  (visible_from objective0 waypoint0)
  (visible_from objective0 waypoint1)
  (visible_from objective0 waypoint2)
  (visible_from objective0 waypoint3)
  (visible_from objective1 waypoint0)
  (visible_from objective1 waypoint1)
  (visible_from objective1 waypoint2)
  (visible_from objective1 waypoint3)
  (at_soil_sample waypoint0)
  (at_rock_sample waypoint1)
  (at_soil_sample waypoint2)
  (at_rock_sample waypoint2)
  (at_soil_sample waypoint3)
  (at_rock_sample waypoint3)
  (channel_free general)
  (at rover0 waypoint3)
  (available rover0)
  (empty rover0store)
  (= (recharges) 0)
  (= (actions) 0)
  (= (energy rover0) 50))
    (:goal (and (communicated_soil_data waypoint2) (communicated_rock_data waypoint3) (communicated_image_data objective1 high_res))
)
)
