/*
 * Copyright 2003-2005 Carnegie Mellon University and Rutgers University
 * Copyright 2007 H�kan Younes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <config.h>
#include "client.h"
#include "states.h"
#include "problems.h"
#include "domains.h"
#include "actions.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cerrno>
#include <cstdio>
#if HAVE_GETOPT_LONG
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <getopt.h>
#else
#include "port/getopt.h"
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>


/* The parse function. */
extern int yyparse();
/* File to parse. */
extern FILE* yyin;
/* Name of current file. */
std::string current_file;
/* Level of warnings. */
int warning_level;
/* Verbosity level. */
int verbosity;

/* arguments */
std::string str_arg_domain_file_name;
std::string str_arg_problem_file_name;
std::string str_arg_problem_name;
std::string str_arg_planner_name;
std::string str_arg_observations_file_name;




/* Parses the given file, and returns true on success. */
static bool read_file(const char* name) {
  yyin = fopen(name, "r");
  if (yyin == 0) {
    std::cerr << "mdpclient:" << name << ": " << strerror(errno)
              << std::endl;
    return false;
  } else {
    current_file = name;
    bool success = (yyparse() == 0);
    fclose(yyin);
    return success;
  }
}

int connect(const char *hostname, int port)
{
  struct hostent *host = ::gethostbyname(hostname);
  if (!host) {
    perror("gethostbyname");
    return -1;
  }

  int sock = ::socket(PF_INET, SOCK_STREAM, 0);
  if (sock == -1) {
    perror("socket");
    return -1;
  }

  struct sockaddr_in addr;
  addr.sin_family=AF_INET;
  addr.sin_port=htons(port);
  addr.sin_addr = *((struct in_addr *)host->h_addr);
  memset(&(addr.sin_zero), '\0', 8);

  if (::connect(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    perror("connect");
    return -1;
  }
  return sock;
  //remember to call close(sock) when you're done
}

class ProbPlanner : public Planner
{
public:
  ProbPlanner(const Problem& problem) : Planner(problem) {}
  ProbPlanner(std::string domain_file, const Problem& problem) : Planner(problem) { 
    domain_file_name_ = domain_file;
    observations_file_name_ = str_arg_observations_file_name;
    egreedyness=1;
  }
  virtual void initRound();
  virtual ~ProbPlanner() {}
  virtual const Action* decideAction(const AtomSet& state_atoms,const ValueMap& state_values);
  virtual void endRound();

private:  
  std::string domain_file_name_;
  std::string observations_file_name_;
  AtomSet static_state_atoms_;
  AtomSet previous_state_atoms_;
  ValueMap previous_state_values_;
  std::vector<std::string> plan_;
  int index_current_action_;

  double egreedyness;

  bool plan(std::string domain_file_name, const AtomSet& state_atoms,const ValueMap& state_values);
  bool plan_ff(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values);
  
  bool replan(const AtomSet& state_atoms,const ValueMap& state_values);
  bool repairplan();
  int generate_reproblem(std::string domain_file, std::string reproblem_domain_file, const AtomSet& state_atoms,const ValueMap& state_values);
  int print_pending_plan();
  int write_Observation(std::string observations_file_name, std::string action, const AtomSet& state_atoms,const ValueMap& state_values, std::string result);
  AtomSet get_previous_state_atoms();
  ValueMap get_previous_state_values();
  void set_previous_state(const AtomSet& state_atoms,const ValueMap& state_values);
};

/* Begin My Functions */

bool ProbPlanner::plan(std::string domain_file_name, const AtomSet& state_atoms,const ValueMap& state_values){
  // Reject the rest of the plan
  std::vector<std::string>::iterator it;
  int i=0;
  for(it=plan_.begin();it!=plan_.end();it++){
    if (i>=index_current_action_) break;
    else i++;
  }
  plan_.erase(it, plan_.end()); 

  // Create the aux domain File
  std::ostringstream os_domain_file_name;
  os_domain_file_name<<"./domain.pddl";
  std::ofstream aux_domain_file (os_domain_file_name.str().c_str());

  // Create the aux problem File
  std::ostringstream os_problem_file_name;
  os_problem_file_name<< "./problem.pddl";
  std::ofstream aux_problem_file (os_problem_file_name.str().c_str());
 
  // Reading the file
  std::ifstream ifs_domain_file (domain_file_name.c_str());
  if (!ifs_domain_file){
    std::cerr << PACKAGE ": error cant open domain file "<< domain_file_name<< std::endl;
    return -1;
  }

  std::string line;
  bool binproblem=false;
  while(!ifs_domain_file.eof()){  
    getline(ifs_domain_file, line);
    if (line.find("(define (problem")!=std::string::npos) binproblem=true;

    if(binproblem==false) aux_domain_file << line << std::endl;
    else aux_problem_file << line << std::endl;
  }

  aux_domain_file.close();
  aux_problem_file.close(); 
  ifs_domain_file.close();  

  //if(str_arg_planner_name=="random")
    //return plan_random(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);
  //if(str_arg_planner_name=="ff")
  return plan_ff(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);
  //if(str_arg_planner_name=="lpg")
    //return plan_lpg(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);
  //if(str_arg_planner_name=="lpgegreedy")
    //return plan_egreedy(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);

  //Online configurations
  //if(str_arg_planner_name=="egreedy1")
    //return plan_random(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);    
  //if(str_arg_planner_name=="egreedy2")
    //return plan_egreedy2(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);
  //if(str_arg_planner_name=="egreedy3")
    //return plan_egreedy3(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);
  //if(str_arg_planner_name=="egreedy4")
    //return plan_egreedy4(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);
  //if(str_arg_planner_name=="egreedy5")
    //return plan_ff(os_domain_file_name.str(),os_problem_file_name.str(), state_atoms,state_values);
}

bool ProbPlanner::plan_ff(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values){  
  // Calling the fragility-ff planner
  std::cout << "planning... " << std::endl;
  std::ostringstream command_os;
  std::string plan_file_name="plan.tmp";
  command_os << "/home/moises/Metric-FF/ff -o " << domain_file_name<< " -f "<< problem_file_name << " -k 1 > " << plan_file_name <<std::endl;
  std::cout << command_os.str().c_str() << std::endl;
  system(command_os.str().c_str());
 
  // Reading the new plan
  std::ifstream plan_file (plan_file_name.c_str());
  if (!plan_file){
    std::cerr << PACKAGE ": error cant open plan file "<< plan_file_name << std::endl;
    return false;
  }
  
  int n_actions=0;
  int ini_pos,end_pos;
  std::string str_action;
  bool bin_the_plan=false;
  std::string line;
  while(!plan_file.eof()){
    // Reading an action and adding it to the plan vector
    getline(plan_file, line);    
    if(line.find("0:")!=std::string::npos) bin_the_plan=true;
    if(line.find(":")!=std::string::npos){
      if (bin_the_plan){
	ini_pos=line.find(": ")+2;
	str_action=line.substr(ini_pos);
	str_action="("+str_action;
	str_action=str_action+")";
	plan_.push_back(str_action);
	n_actions++;    	
      }     
    }else bin_the_plan=false;
  }
  plan_file.close();
  
  if(n_actions==0) return false;
  
  return true;
}

int ProbPlanner::generate_reproblem(std::string domain_file, std::string reproblem_domain_file, const AtomSet& state_atoms,const ValueMap& state_values){

  //openning the original domain file
  std::ifstream ifs_domain_file (domain_file.c_str());
  if (!ifs_domain_file){
    std::cerr << PACKAGE ": error cant open domain file "<< domain_file << std::endl;
    return -1;
  }
  
  //creating the replanning domain file
  std::ofstream ofs_reproblem_domain_file (reproblem_domain_file.c_str());  
  std::string line;
  while(!ifs_domain_file.eof()){
    // Reading
    getline(ifs_domain_file, line);
    if (line.find("(define (problem")!=std::string::npos) break;
    ofs_reproblem_domain_file << line << std::endl;
  }
  ifs_domain_file.close();
  ofs_reproblem_domain_file << std::endl;

  //creating the reproblem
  ofs_reproblem_domain_file << "(define (problem " << _problem.name() << ")" << std::endl;
  ofs_reproblem_domain_file << "    (:domain " << _problem.domain().name() << ")" << std::endl;
  ofs_reproblem_domain_file << "    (:objects" << _problem.terms()  << ") " << std::endl;
  
  ofs_reproblem_domain_file << "    (:init (= (fragility) 0) ";
  for (AtomSet::const_iterator ai = static_state_atoms_.begin();
       ai != static_state_atoms_.end(); ai++) {
    ofs_reproblem_domain_file << std::endl << "  " << **ai;
  }
  for (AtomSet::const_iterator ai = state_atoms.begin();
       ai != state_atoms.end(); ai++) {
    ofs_reproblem_domain_file << std::endl << "  " << **ai;
  }
  for (ValueMap::const_iterator vi = state_values.begin();
       vi != state_values.end(); vi++) {
    ofs_reproblem_domain_file << std::endl << "  (= " << *(*vi).first << ' ' << (*vi).second << ")";
  }
    
  ofs_reproblem_domain_file << ")" << std::endl;
  
  ofs_reproblem_domain_file << "    (:goal ";
  ofs_reproblem_domain_file << _problem.goal()<< std::endl;
  ofs_reproblem_domain_file << ")" << std::endl;

  ofs_reproblem_domain_file <<"(:metric minimize (fragility))" << std::endl;

  ofs_reproblem_domain_file << ")" << std::endl;
  ofs_reproblem_domain_file.close();

  return 0;
}

bool ProbPlanner::replan(const AtomSet& state_atoms,const ValueMap& state_values){  
  std::string reproblem_domain_file = "reproblem-domain.pddl";
  generate_reproblem(str_arg_domain_file_name, reproblem_domain_file, state_atoms, state_values);
  return plan(reproblem_domain_file, state_atoms, state_values);
}


bool ProbPlanner::repairplan(){
  std::cout << "Repairing plan... " << std::endl; 
  return false;
}

int ProbPlanner::print_pending_plan()
{
  std::vector<std::string>::iterator it;
  int i=index_current_action_;
  std::cout << "The pending plan: " << std::endl;
  for(it=plan_.begin()+index_current_action_;it!=plan_.end();it++){
    std::cout << i<< ": " << *it <<" " << std::endl;
    i++;
  }
  return 0;
}

int ProbPlanner::write_Observation(std::string observations_file_name, std::string action, const AtomSet& state_atoms,const ValueMap& state_values, std::string result)
{
  using namespace std;

  std::ofstream file_observations;
  file_observations.open(observations_file_name.c_str(), std::ios::app);

  
  std::ostringstream state_os; 

  for (AtomSet::const_iterator ai = static_state_atoms_.begin();
       ai != static_state_atoms_.end(); ai++) {
    state_os <<" " << **ai;
  }
  
  for (AtomSet::const_iterator ai = state_atoms.begin();
       ai != state_atoms.end(); ai++) {
    state_os <<" " << **ai;    
  }
  /*
  for (ValueMap::const_iterator vi = state_values.begin();
       vi != state_values.end(); vi++) {
    state_os << std::endl << "  (= " << *(*vi).first << ' ' << (*vi).second << ")";
  }
  */
  
  file_observations << action << "|" << result <<"|"<< state_os.str() << "|" << std::endl;  
  file_observations.close();
  return 0;
}

AtomSet ProbPlanner::get_previous_state_atoms(){
  return previous_state_atoms_;
}

ValueMap ProbPlanner::get_previous_state_values(){
  return previous_state_values_;
}

void ProbPlanner::set_previous_state(const AtomSet& state_atoms,const ValueMap& state_values){

  previous_state_atoms_.clear();
  for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++) {
    previous_state_atoms_.insert(*ai);
  }
  
  for (ValueMap::const_iterator vi = state_values.begin();vi != state_values.end(); vi++){    
    if (previous_state_values_.find((*vi).first) == previous_state_values_.end()) {
      previous_state_values_.insert(std::make_pair((*vi).first, (*vi).second));
      RCObject::ref((*vi).first);
    } else {
      previous_state_values_[(*vi).first] = (*vi).second;
    }
  }
}

/* End My Functions */
void ProbPlanner::initRound()
{
  plan_.clear();
  index_current_action_=0;
  srand((unsigned int)time(NULL)); 
  
  // getting the initial State 
  set_previous_state(_problem.init_atoms(), _problem.init_values());

  if(plan(domain_file_name_, _problem.init_atoms(), _problem.init_values())==false){
    std::cerr << PACKAGE ": NO plan found for the problem: " << std::endl;
    return;
  }
}


void ProbPlanner::endRound()
{
}

const Action* ProbPlanner::decideAction(const AtomSet& state_atoms,const ValueMap& state_values)
{
  std::string previous_action="";

  // ------- Getting the static atoms ---------
  if(index_current_action_==0){
    //Get the dynamic atoms
    std::ostringstream os_dymanic_atoms;
    for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++){
      os_dymanic_atoms << **ai;
    }
    for (AtomSet::const_iterator ai = _problem.init_atoms().begin();
	 ai != _problem.init_atoms().end(); ai++) {
      std::ostringstream os_atom_aux;
      os_atom_aux << **ai;
      if(os_dymanic_atoms.str().find(os_atom_aux.str())==std::string::npos){
	static_state_atoms_.insert(*ai);
      }
    }
  }
  
  // ------- Writting the observation of the Previous Action ---------
  if(index_current_action_>0){
    
    previous_action=plan_.at(index_current_action_-1);
    
    // Success or Failure or Dead Deadend
    bool success;
    const ActionSet *allactions;
    allactions=&_problem.actions();   
    AtomSet predicted_state_atoms;
    ValueMap predicted_state_values;
    
    //Getting the predicted state
    for (ActionSet::const_iterator ai = allactions->begin();ai != allactions->end(); ai++) {     
      std::ostringstream os_aux;
      os_aux<<**ai;
      if(strcasecmp(os_aux.str().c_str(), previous_action.c_str())==0){
        predicted_state_atoms = get_previous_state_atoms();
	predicted_state_values = get_previous_state_values();
	(*ai)->affect(_problem.terms(),predicted_state_atoms, predicted_state_values);
      }
    }    
    
    // Comparing the predicted and the real state
    success=true;
    std::ostringstream os_real_state_aux;
    //Dynamic Atoms
    for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++) {	 	  
      os_real_state_aux << **ai;
    }
    //Static Atoms
    for (AtomSet::const_iterator ai = static_state_atoms_.begin();ai != static_state_atoms_.end(); ai++) {	 	  
      os_real_state_aux << **ai;
    }
    std::ostringstream os_predicted_state_aux;
    for (AtomSet::const_iterator ai = predicted_state_atoms.begin();ai != predicted_state_atoms.end(); ai++) {	 	  
      os_predicted_state_aux << **ai;
    }    

    for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++) {
      std::ostringstream os_real_atom_aux;	  
      os_real_atom_aux << **ai;	  	  	  
      if(os_predicted_state_aux.str().find(os_real_atom_aux.str())==std::string::npos) success=false;
    }
    
    for (AtomSet::const_iterator ai = predicted_state_atoms.begin();ai != predicted_state_atoms.end(); ai++) {
      std::ostringstream os_predicted_atom_aux;	  
      os_predicted_atom_aux << **ai;	  
      if(os_real_state_aux.str().find(os_predicted_atom_aux.str())==std::string::npos) success=false;
    }  
    
    if(success==true){
      write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "success");
    }else{ 
      // No Plan Repair => Dead End     
      if(repairplan()==false){      
	if(replan(state_atoms, state_values)==false){
	  std::cerr << PACKAGE ": No plan found for the new State: " << std::endl;
	  std::cerr << PACKAGE ": Execution Dead End" << std::endl;
	  write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "deadend");      	
	  return NULL;
	}else{
	  // Plan Repair => Failure
	  write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "failure");      
	}
      }else{
	write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "failure");	      	
      }      
    }
  }

  if(index_current_action_ == plan_.size()){
    std::cout<< "No more actions in the plan " << std::endl;
    if(repairplan()==false){
      if(replan(state_atoms,state_values)==false){
	return NULL;
      }
    }
  }         
  
  // Getting the action to execute        
  ActionList actions;
  _problem.enabled_actions(actions, state_atoms, state_values);
  
  //Only for egreedy: Choosing a random action from the enabled_actions vector 
  double aleat=1.0*rand()/(RAND_MAX + 1.0);  
  if((egreedyness<1)&&(aleat>egreedyness)){
    std::cout << "Taking random decision... " << std::endl;
    unsigned i_aleat = size_t(rand()/(RAND_MAX + 1.0)*actions.size());    
    std::ostringstream os_aux2;
    os_aux2<<*actions[i_aleat];
    plan_[index_current_action_]=os_aux2.str();      
    index_current_action_++;
    std::cout<< "Sent action " << index_current_action_<< ": " << os_aux2.str() << std::endl;  
    set_previous_state(state_atoms,state_values);
    
    // Reject the rest of the plan  
    std::vector<std::string>::iterator it;
    int i=0;
    for(it=plan_.begin();it!=plan_.end();it++){
      if (i>=index_current_action_) break;
      else i++;
    }
    plan_.erase(it, plan_.end());     
    return actions[i_aleat];
  }

  std::string next_action="";  
  next_action=plan_.at(index_current_action_);
  //Looking for the next action in the enabled_actions vector    
  for (int i=0; i<actions.size(); i++){
    std::ostringstream os_aux;
    os_aux<<*(actions[i]);    
    if(strcasecmp(os_aux.str().c_str(), next_action.c_str())==0){
      index_current_action_++;           
      std::cout<< "Sent action " << index_current_action_ << ": " << os_aux.str()  << std::endl;
      set_previous_state(state_atoms,state_values);      
      return actions[i];      
    }
  }  
    
  // Next action is wrong (there must be an error)
  std::cout<< "The action " << next_action << " Can't be executed !!" << std::endl;  
  return NULL;
}


int main(int argc, char **argv)
{
  /* Set default verbosity. */
  verbosity = 1;
  /* Set default warning level. */
  warning_level = 1;
  if (argc != 6) {
    std::cerr << "Usage: " << *argv << " <host>:<port> <domain file> <problem file> <problem name> <observations file>" << std::endl;
    return 1;
  }

  if (!read_file(argv[3])) {
    std::cerr << "Couldn't read problem file " << argv[2] << std::endl;
    return 1;
  }

  const Problem *problem = Problem::find(argv[4]);

  if (!problem) 
  {
    std::cerr << "Problem " << argv[4] << " is not defined in " << argv[3] << std::endl;
    
    //return 1;
  }

  //Capturing the arguments
  str_arg_domain_file_name		= argv[2];
  str_arg_problem_file_name		= argv[3];
  str_arg_problem_name			= argv[4];
  str_arg_observations_file_name	= argv[5];
  
  char *hostport = argv[1];
  char *host = strtok(hostport, ":");
  char *portstr = strtok(0, ":");
  int port = atoi(portstr);

  try{
    
    int socket = connect(host, port);
    if (socket <= 0) {
      std::cerr << "Could not connect to " << host << ':' << port << std::endl;
      return 1;
    }

    std::string prob_domain_file=argv[2];    
    ProbPlanner pplanner(prob_domain_file, *problem);          
    //XMLClient(pplanner, *problem, "result", socket);
    
  } catch (const std::exception& e) {
    std::cerr << std::endl << "mdpclient: " << e.what() << std::endl;
    return 1;
  } catch (...) {
    std::cerr << "mdpclient: fatal error" << std::endl;
    return -1;
  }
  
  Problem::clear();
  Domain::clear();
  
  return 0;
}
