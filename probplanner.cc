/* -*-C++-*- */
/*
 * Probalistic Planner.
 *
 * Copyright 2011 Moisés Martínez Muñoz Universidad Carlos III Madrid
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "mdpplanner.h"


bool ProbPlanner::plan(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values)
{
  // Reject the rest of the plan
  std::vector<std::string>::iterator it;
  int i=0;

  for(it=plan_.begin();it!=plan_.end();it++)
  {
    if (i>=index_current_action_) break;
    else i++;
  }
  
  plan_.erase(it, plan_.end()); 

  // Create the aux domain File
  //std::ostringstream os_domain_file_name;
  //os_domain_file_name << "./domain.pddl";
  //std::ofstream aux_domain_file (os_domain_file_name.str().c_str());

  // Create the aux problem File
  //std::ostringstream os_problem_file_name;
  //os_problem_file_name << "./problem.pddl";
  //std::ofstream aux_problem_file (os_problem_file_name.str().c_str());
 
  // Reading the file
  //std::ifstream ifs_domain_file (domain_file_name.c_str());
  //if (!ifs_domain_file){
  //  std::cerr << PACKAGE ": error cant open domain file "<< domain_file_name<< std::endl;
  //  return -1;
  //}

  //std::string line;
  //bool binproblem=false;
  //while(!ifs_domain_file.eof()){  
  //  getline(ifs_domain_file, line);
  //  if (line.find("(define (problem")!=std::string::npos) binproblem=true;

  //  if(binproblem==false) aux_domain_file << line << std::endl;
  //  else aux_problem_file << line << std::endl;
  //}

  //aux_domain_file.close();
  //aux_problem_file.close(); 
  //ifs_domain_file.close();  

  return plan_ff(domain_file_name, /*os_problem_file_name.str()*/problem_file_name, state_atoms, state_values);
}

bool ProbPlanner::plan_ff(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values)
{  

  std::cout << "planning... " << std::endl;
  std::ostringstream command_os;
  std::string plan_file_name="plan.tmp";

  command_os << "../planners/Metric-FF/ff -o " << domain_file_name<< " -f "<< problem_file_name << " > " << plan_file_name <<std::endl;

  std::cout << command_os.str().c_str() << std::endl;
  system(command_os.str().c_str());
 
  // Reading the new plan
  std::ifstream plan_file (plan_file_name.c_str());

  if (!plan_file)
  {
    std::cerr << PACKAGE ": error cant open plan file "<< plan_file_name << std::endl;
    return false;
  }
  
  int n_actions = 0;
  int ini_pos,end_pos;
  std::string str_action;
  bool bin_the_plan=false;
  std::string line;
  
  while(!plan_file.eof())
  {
    // Reading an action and adding it to the plan vector
    getline(plan_file, line);    

    std::cout << line << std::endl;

    if(line.find("0:")!=std::string::npos) bin_the_plan=true;
  
    if(line.find(":")!=std::string::npos){
      if (bin_the_plan){
	ini_pos=line.find(": ")+2;
	str_action=line.substr(ini_pos);
	str_action="("+str_action;
	str_action=str_action+")";
	plan_.push_back(str_action);
	n_actions++;    	
      }     
    }else bin_the_plan=false;
  }
  plan_file.close();

  exit(1);
  
  if(n_actions==0) return false;
  
  return true;
}

int ProbPlanner::generate_reproblem(std::string domain_file, std::string reproblem_domain_file, const AtomSet& state_atoms,const ValueMap& state_values){

  //openning the original domain file
  std::ifstream ifs_domain_file (domain_file.c_str());
  if (!ifs_domain_file){
    std::cerr << PACKAGE ": error cant open domain file "<< domain_file << std::endl;
    return -1;
  }
  
  //creating the replanning domain file
  std::ofstream ofs_reproblem_domain_file (reproblem_domain_file.c_str());  
  std::string line;
  while(!ifs_domain_file.eof()){
    // Reading
    getline(ifs_domain_file, line);
    if (line.find("(define (problem")!=std::string::npos) break;
    ofs_reproblem_domain_file << line << std::endl;
  }
  ifs_domain_file.close();
  ofs_reproblem_domain_file << std::endl;

  //creating the reproblem
  ofs_reproblem_domain_file << "(define (problem " << _problem.name() << ")" << std::endl;
  ofs_reproblem_domain_file << "    (:domain " << _problem.domain().name() << ")" << std::endl;
  ofs_reproblem_domain_file << "    (:objects" << _problem.terms()  << ") " << std::endl;
  
  //ofs_reproblem_domain_file << "    (:init (= (fragility) 0) ";
  ofs_reproblem_domain_file << "    (:init ";
  
  for (AtomSet::const_iterator ai = static_state_atoms_.begin();
       ai != static_state_atoms_.end(); ai++) {
    ofs_reproblem_domain_file << std::endl << "  " << **ai;
  }
  for (AtomSet::const_iterator ai = state_atoms.begin();
       ai != state_atoms.end(); ai++) {
    ofs_reproblem_domain_file << std::endl << "  " << **ai;
  }
  for (ValueMap::const_iterator vi = state_values.begin();
       vi != state_values.end(); vi++) {
    ofs_reproblem_domain_file << std::endl << "  (= " << *(*vi).first << ' ' << (*vi).second << ")";
  }
    
  ofs_reproblem_domain_file << ")" << std::endl;
  
  ofs_reproblem_domain_file << "    (:goal ";
  ofs_reproblem_domain_file << _problem.goal()<< std::endl;
  ofs_reproblem_domain_file << ")" << std::endl;

  //ofs_reproblem_domain_file <<"(:metric minimize (fragility))" << std::endl;

  ofs_reproblem_domain_file << ")" << std::endl;
  ofs_reproblem_domain_file.close();

  return 0;
}

bool ProbPlanner::replan(const AtomSet& state_atoms,const ValueMap& state_values){  
  std::string reproblem_domain_file = "reproblem-domain.pddl";
  generate_reproblem(str_arg_domain_file_name, reproblem_domain_file, state_atoms, state_values);
  return plan(reproblem_domain_file, state_atoms, state_values);
}


bool ProbPlanner::repairplan(){
  std::cout << "Repairing plan... " << std::endl; 
  return false;
}

int ProbPlanner::print_pending_plan()
{
  std::vector<std::string>::iterator it;
  int i=index_current_action_;
  std::cout << "The pending plan: " << std::endl;
  for(it=plan_.begin()+index_current_action_;it!=plan_.end();it++){
    std::cout << i<< ": " << *it <<" " << std::endl;
    i++;
  }
  return 0;
}

int ProbPlanner::write_Observation(std::string observations_file_name, std::string action, const AtomSet& state_atoms,const ValueMap& state_values, std::string result)
{
  using namespace std;

  std::ofstream file_observations;
  file_observations.open(observations_file_name.c_str(), std::ios::app);

  
  std::ostringstream state_os; 

  for (AtomSet::const_iterator ai = static_state_atoms_.begin();
       ai != static_state_atoms_.end(); ai++) {
    state_os <<" " << **ai;
  }
  
  for (AtomSet::const_iterator ai = state_atoms.begin();
       ai != state_atoms.end(); ai++) {
    state_os <<" " << **ai;    
  }
  /*
  for (ValueMap::const_iterator vi = state_values.begin();
       vi != state_values.end(); vi++) {
    state_os << std::endl << "  (= " << *(*vi).first << ' ' << (*vi).second << ")";
  }
  */
  
  file_observations << action << "|" << result <<"|"<< state_os.str() << "|" << std::endl;  
  file_observations.close();
  return 0;
}

AtomSet ProbPlanner::get_previous_state_atoms(){
  return previous_state_atoms_;
}

ValueMap ProbPlanner::get_previous_state_values(){
  return previous_state_values_;
}

void ProbPlanner::set_previous_state(const AtomSet& state_atoms,const ValueMap& state_values){

  previous_state_atoms_.clear();
  for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++) {
    previous_state_atoms_.insert(*ai);
  }
  
  for (ValueMap::const_iterator vi = state_values.begin();vi != state_values.end(); vi++){    
    if (previous_state_values_.find((*vi).first) == previous_state_values_.end()) {
      previous_state_values_.insert(std::make_pair((*vi).first, (*vi).second));
      RCObject::ref((*vi).first);
    } else {
      previous_state_values_[(*vi).first] = (*vi).second;
    }
  }
}
