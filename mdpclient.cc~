/*
 * Copyright 2003-2005 Carnegie Mellon University and Rutgers University
 * Copyright 2007 H�kan Younes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>


#include <config.h>
#include "client.h"
#include "states.h"
#include "problems.h"
#include "domains.h"
#include "actions.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cerrno>
#include <cstdio>
#if HAVE_GETOPT_LONG
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <getopt.h>
#else
#include "port/getopt.h"
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/times.h>
#include <time.h>

/* The parse function. */
extern int yyparse();
/* File to parse. */
extern FILE* yyin;
/* Name of current file. */
std::string current_file;
/* Level of warnings. */
int warning_level;
/* Verbosity level. */
int verbosity;

/* arguments */
std::string str_arg_domain_name;
std::string str_arg_domain_file_name;
std::string str_arg_problem_file_name;
std::string str_arg_abstract_file_name;
std::string str_arg_problem_name;
std::string str_arg_planner_name = "ff";
std::string str_arg_observations_file_name;

std::stringstream exp_file_name;
std::stringstream obs_file_name;

std::string output_file_name;

double planning_time 		= 0;

bool bool_arg_evolution		= true;

int int_arg_horizon 		= 0;
int int_arg_heuristic		= 1;
int int_arg_planner		= 2;
int int_arg_noabs		= 0;
int success_actions 		= 0;
int failure_actions 		= 0;
int deadend_actions 		= 0;

int execution 				= 0;

struct tms start, end;


/* Parses the given file, and returns true on success. */
static bool read_file(const char* name) 
{
  yyin = fopen(name, "r");
  
  if (yyin == 0) 
  {
    std::cerr << "mdpclient:" << name << ": " << strerror(errno) << std::endl;
    return false;
  } 
  else 
  {
    current_file = name;

    bool success = (yyparse() == 0);
    fclose(yyin);
    return success;
  }
}

int connect(const char *hostname, int port)
{
  struct hostent *host = ::gethostbyname(hostname);
  struct sockaddr_in addr;
  
  if (!host) 
  {
    perror("gethostbyname");
    return -1;
  }

  int sock = ::socket(PF_INET, SOCK_STREAM, 0);
  
  if (sock == -1) 
  {
    perror("socket");
    return -1;
  }

  
  addr.sin_family	= AF_INET;
  addr.sin_port		= htons(port);
  addr.sin_addr 	= *((struct in_addr *)host->h_addr);
  memset(&(addr.sin_zero), '\0', 8);

  if (::connect(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1) 
  {
    perror("connect");
    return -1;
  }
 
  return sock;
}

class ProbPlanner : public Planner
{

  public:
  	ProbPlanner(const Problem& problem) : Planner(problem) {}
  	ProbPlanner(std::string domain_file, std::string abstract_file, std::string obs_file, std::string exp_file, const Problem& problem) : Planner(problem) 
	{ 

		domain_file_name_ 		= domain_file;
		abstract_file_name_		= abstract_file;
		observations_file_name_	= obs_file;
      experiments_file_name_	= exp_file;
  	}

        std::string getExperimentsFileName() {return experiments_file_name_;}
  
  	virtual ~ProbPlanner() {}
  	virtual const Action* decideAction(const AtomSet& state_atoms,const ValueMap& state_values);
	virtual void initRound();  	
	virtual void endRound();

  private:

	std::string domain_file_name_;
	std::string abstract_file_name_;
	std::string observations_file_name_;
        std::string experiments_file_name_;
  
	//Private variable

	AtomSet static_state_atoms_;
  	AtomSet previous_state_atoms_;
  	ValueMap previous_state_values_;
  	std::vector<std::string> plan_;
  	
	int index_current_action_;

	void set_previous_state(const AtomSet& state_atoms,const ValueMap& state_values);

  	bool plan(std::string domain_file_name, std::string abs_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values);
  	bool plan_ff(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values);
	bool plan_fd(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values);
  	bool replan(const AtomSet& state_atoms,const ValueMap& state_values);
  	bool repairplan();

  	//int generate_reproblem(std::string domain_file, std::string reproblem_domain_file, const AtomSet& state_atoms,const ValueMap& state_values);
  	int generate_reproblem(std::string domain_file, const AtomSet& state_atoms,const ValueMap& state_values);
  	int print_pending_plan();
  	int write_Observation(std::string observations_file_name, std::string action, const AtomSet& state_atoms,const ValueMap& state_values, std::string result);
	int print_plan(std::string action);
  
	AtomSet get_previous_state_atoms();
  	ValueMap get_previous_state_values();
  	
};

/* Begin My Functions */

bool ProbPlanner::plan(std::string domain_file_name, std::string abs_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values)
{
  // Reject the rest of the plan

  std::vector<std::string>::iterator it;
  int i	= 0;

  for(it=plan_.begin(); it!=plan_.end(); it++)
  {
    if (i >= index_current_action_) 
	break;
    else 
	i++;
  }
  
  plan_.erase(it, plan_.end());  

  if (int_arg_planner == 1)
	return plan_ff(domain_file_name, problem_file_name, state_atoms, state_values);
  else
	return plan_fd(domain_file_name, problem_file_name, state_atoms, state_values);
  
}

bool ProbPlanner::plan_ff(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values)
{  

  	std::ostringstream 	command_os;
	std::ostringstream	command_os2;
  	

	std::string 	plan_file_name = "plan_final.tmp";
  	std::string 	str_action;
	std::string	str_time;
  	std::string 	line;

  	int n_actions		= 0;
  	int ini_pos;
	int end_pos;
  
  	bool bin_the_plan	= false;

	double time		= 0;
	double local_time 	= 0;

	command_os << "../planners/Metric-FF/ff -o " << domain_file_name << " -f " << problem_file_name << " -k " << int_arg_horizon << std::endl;

	system(command_os.str().c_str());

  	// Reading the new plan
  	std::ifstream plan_file ("plan_final.tmp");

  	if (!plan_file)
  	{
    	std::cerr << PACKAGE ": error cant open plan file "<< plan_file_name << std::endl;
    	return false;
  	}
  
	while (!plan_file.eof())
  	{
		// Reading an action and adding it to the plan vector
		getline(plan_file, line);

	  	if (line.find("0:") != std::string::npos) bin_the_plan = true;
	  
		if (line.find(":") != std::string::npos)
		{
			//std::cout << "1:" << line << std::endl;

			if (bin_the_plan)
		   {
				ini_pos = line.find(": ")+2;
				str_action = line.substr(ini_pos);
				str_action = "("+str_action;
				str_action = str_action+")";

				//std::cout << str_action << std::endl;

				plan_.push_back(str_action);
				n_actions++;    	
		   }     
		}
		else 
		{
			//std::cout << "2:" << line << std::endl;

			if (line.find("=") != std::string::npos)
		   {
				str_time = line.substr((line.find("=") + 1));
				std::istringstream(str_time) >> time;
				
				/*std::cout << "TIEMPO: " << time << std::endl;*/

				planning_time += (time < 1) ? 1:time;
				local_time += (time < 1) ? 1:time;
			}
			else
				bin_the_plan = false;
	  	}
	}

  	plan_file.close();

	if ((n_actions > 0) && (bool_arg_evolution))
	{
		std::ofstream values;

		values.open("../incremental", std::ios::app);
		values << execution << " " << n_actions << " " << local_time << std::endl;  
  		values.close();

		execution++;
	}
  
  	if (n_actions==0) return false;
  
  	return true;
}

bool ProbPlanner::plan_fd(std::string domain_file_name, std::string problem_file_name, const AtomSet& state_atoms,const ValueMap& state_values)
{  

  	std::ostringstream 	command_os;
	std::ostringstream	command_os2;
	std::ostringstream	command_os3;  	

	std::string 	plan_file_name = "plan_final.tmp";
  	std::string 	str_action;
	std::string	str_horizon;
	std::string	str_time;
  	std::string 	line;

  	int n_actions		= 0;
  	int ini_pos;
	int end_pos;
  
  	bool bin_the_plan	= false;

	double time			= 0;
	double local_time = 0;
  

	command_os << "../planners/FD/translate/translate.py " << domain_file_name << " " << problem_file_name << " > data,txt" << std::endl;
	
   	std::cout << command_os.str().c_str() << std::endl;

   	system(command_os.str().c_str());

	command_os2 << "../planners/FD/preprocess/preprocess < output.sas > data.txt" << std::endl;
	system(command_os2.str().c_str());

	std::cout << command_os.str().c_str() << std::endl;

	switch (int_arg_heuristic)
	{
		case 1:	if (int_arg_horizon != 0)
			{
				if (int_arg_noabs == 1)	
					command_os3 << "../planners/FD/search/downward --search \"astar(ff())\" --horizon " << int_arg_horizon << " --noabs 1 < output > data.txt" << std::endl;
				else
					command_os3 << "../planners/FD/search/downward --search \"astar(ff())\" --horizon " << int_arg_horizon << " < output > data.txt" << std::endl;
			}
			else
				command_os3 << "../planners/FD/search/downward --search \"astar(ff())\" < output > data.txt" << std::endl;
			break;
		case 2:	if (int_arg_horizon != 0)
			{
				if (int_arg_noabs == 1)	
					command_os3 << "../planners/FD/search/downward --search \"astar(lmcut())\" --horizon " << int_arg_horizon << " --noabs 1 < output > data.txt" << std::endl;
				else
					command_os3 << "../planners/FD/search/downward --search \"astar(lmcut())\" --horizon " << int_arg_horizon << " < output > data.txt" << std::endl;
			}
			else
				command_os3 << "../planners/FD/search/downward --search \"astar(lmcut())\" < output > data.txt" << std::endl;
			break;
	}

	std::cout << command_os3.str().c_str() << std::endl;

	system(command_os3.str().c_str());

  	// Reading the new plan
  	std::ifstream plan_file ("sas_plan");


  	if (!plan_file)
  	{
    	std::cerr << PACKAGE ": error cant open plan file "<< plan_file_name << std::endl;
    	return false;
  	}
  
	while (!plan_file.eof())
  	{
		// Reading an action and adding it to the plan vector
		getline(plan_file, line);		

		if (line.find("(") != std::string::npos)
		{
		  	plan_.push_back(line);
			n_actions++;    
		}
		else
		{
			//std::cout << "2:" << line << std::endl;

			if (line.find("=") != std::string::npos)
			{
				str_time = line.substr((line.find("=") + 1));
				std::istringstream(str_time) >> time;
				
				/*std::cout << "TIEMPO: " << time << std::endl;*/

				planning_time += (time < 1) ? 1:time;
				local_time += (time < 1) ? 1:time;
			}
			else
				bin_the_plan = false;
	  	}	

	}

  	plan_file.close();

	if ((n_actions > 0) && (bool_arg_evolution))
	{
		std::ofstream values;

		values.open("../incremental", std::ios::app);
		values << execution << " " << n_actions << " " << local_time << std::endl;  
  		values.close();

		execution++;
	}
  
  	if (n_actions==0) return false;
  
  	return true;
}

int ProbPlanner::generate_reproblem(std::string domain_file, const AtomSet& state_atoms,const ValueMap& state_values)
{
  std::ostringstream aux_string;

  //std::cout << "GENERATE PROBLEM" << std::endl;

  std::ofstream ofs_reproblem_file(domain_file.c_str());  

  //creating the reproblem

  ofs_reproblem_file << "(define (problem " << _problem.name() << ")" << std::endl;
  ofs_reproblem_file << "    (:domain " << _problem.domain().name() << ")" << std::endl;
  ofs_reproblem_file << "    (:objects" << _problem.terms()  << ") " << std::endl;
  
  ofs_reproblem_file << "    (:init ";
  
  for (AtomSet::const_iterator ai = static_state_atoms_.begin(); ai != static_state_atoms_.end(); ai++) 
  {
	ofs_reproblem_file << std::endl << "  " << **ai;
  }

  for (AtomSet::const_iterator ai = state_atoms.begin(); ai != state_atoms.end(); ai++) 
  {
	ofs_reproblem_file << std::endl << "  " << **ai;
  }
  
  for (ValueMap::const_iterator vi = state_values.begin(); vi != state_values.end(); vi++) 
  {
    	ofs_reproblem_file << std::endl << "  (= " << *(*vi).first << ' ' << (*vi).second << ")";
  }
    
  ofs_reproblem_file << ")" << std::endl;
  
  ofs_reproblem_file << "    (:goal ";
  ofs_reproblem_file << _problem.goal()<< std::endl;
  ofs_reproblem_file << ")" << std::endl;

  ofs_reproblem_file << ")" << std::endl;
  ofs_reproblem_file.close();

  return 0;
}

bool ProbPlanner::replan(const AtomSet& state_atoms,const ValueMap& state_values)
{  
  std::string reproblem_file = "problem.pddl";

  //std::cout << "REPLANING" << std::endl; 

  generate_reproblem(reproblem_file, state_atoms, state_values);

  return plan(str_arg_domain_file_name, str_arg_abstract_file_name, reproblem_file, state_atoms, state_values);

}

bool ProbPlanner::repairplan()
{
  //std::cout << "Repairing plan... " << std::endl; 
  return false;
}


int ProbPlanner::print_pending_plan()
{
  std::vector<std::string>::iterator it;
  int i	= index_current_action_;


  for(it=plan_.begin()+index_current_action_; it!=plan_.end(); it++)
  {
    std::cout << i<< ": " << *it <<" " << std::endl;
    i++;
  }

  return 0;
}

int ProbPlanner::print_plan(std::string action)
{
	std::ofstream file_observations;
  	file_observations.open("final_plan.tmp", std::ios::app);

	file_observations << action << std::endl;  
	file_observations.close();
	
	return 0;
}

int ProbPlanner::write_Observation(std::string observations_file_name, std::string action, const AtomSet& state_atoms,const ValueMap& state_values, std::string result)
{
  using namespace std;

  std::ofstream file_observations;
  file_observations.open(observations_file_name.c_str(), std::ios::app);

  
  std::ostringstream state_os; 

  for (AtomSet::const_iterator ai = static_state_atoms_.begin();
       ai != static_state_atoms_.end(); ai++) {
    state_os <<" " << **ai;
  }
  
  for (AtomSet::const_iterator ai = state_atoms.begin();
       ai != state_atoms.end(); ai++) {
    state_os <<" " << **ai;    
  }

  /*
  for (ValueMap::const_iterator vi = state_values.begin();
       vi != state_values.end(); vi++) {
    state_os << std::endl << "  (= " << *(*vi).first << ' ' << (*vi).second << ")";
  }
  */
  
  file_observations << action << "|" << result <<"|"<< state_os.str() << "|" << std::endl;  
  file_observations.close();
  return 0;
}

AtomSet ProbPlanner::get_previous_state_atoms()
{
  return previous_state_atoms_;
}

ValueMap ProbPlanner::get_previous_state_values()
{
  return previous_state_values_;
}

void ProbPlanner::set_previous_state(const AtomSet& state_atoms,const ValueMap& state_values)
{
  previous_state_atoms_.clear();
 
  for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++) 
  {
    previous_state_atoms_.insert(*ai);
  }
  
  for (ValueMap::const_iterator vi = state_values.begin();vi != state_values.end(); vi++)
  {    
    if (previous_state_values_.find((*vi).first) == previous_state_values_.end()) 
    {
      previous_state_values_.insert(std::make_pair((*vi).first, (*vi).second));
      RCObject::ref((*vi).first);
    } 
    else 
    {
      previous_state_values_[(*vi).first] = (*vi).second;
    }
  }
}

/* End My Functions */

void ProbPlanner::initRound()
{
  	plan_.clear();
  	index_current_action_=0;
  	srand((unsigned int)time(NULL)); 

	if (bool_arg_evolution)
	{
		std::ofstream values;

		values.open("../incremental", std::ios::app);
		values << "INIT ROUND" << std::endl;  
  		values.close();

		execution = 1;
	}

	planning_time 		= 0.0;
	success_actions	= 0;
	failure_actions	= 0;
	deadend_actions	= 0;

  	// getting the initial State 
  	set_previous_state(_problem.init_atoms(), _problem.init_values());

  	if(plan(str_arg_domain_file_name, str_arg_abstract_file_name, str_arg_problem_file_name, _problem.init_atoms(), _problem.init_values())==false)
  	{
   	std::cerr << PACKAGE ": NO plan found for the problem: " << std::endl;
    	return;
  	}
}

void ProbPlanner::endRound()
{	
	std::ofstream file_experiments;

	std::cout << "FINNISH ROUND" << std::endl;
	
        file_experiments.open(experiments_file_name_.c_str(), std::ios::app);

        file_experiments << "Begin result experiment" << std::endl;
		  file_experiments << "Domain file: " << str_arg_domain_file_name << std::endl;
		  file_experiments << "Domain name: " << str_arg_domain_name << std::endl;
		  file_experiments << "Problem name: " << str_arg_problem_name << std::endl;
        file_experiments << "Horizon: " << int_arg_horizon << std::endl;
        file_experiments << "Planinng time: " << planning_time << std::endl;
        file_experiments << "Execution: " << execution << std::endl;
        file_experiments << "Success: " << success_actions << std::endl;
        file_experiments << "Failure: " << failure_actions << std::endl;
        file_experiments << "Deadend: " << deadend_actions << std::endl;

	if (int_arg_planner == 2)
		file_experiments << "Planner: Fast Downward" << std::endl;
	else
		file_experiments << "Planner: Metric FF" << std::endl;

	switch (int_arg_heuristic)
	{
		case 1: file_experiments << "Heuristic: FF" << std::endl;
			break;
		case 2: file_experiments << "Heuristic: LM cut" << std::endl;
			break;
	}

        file_experiments << "End result experiment" << std::endl << std::endl;
 
  	file_experiments.close();

	if (bool_arg_evolution)
	{
		std::ofstream values;

		values.open("../incremental", std::ios::app);
		        values << "FINISH ROUND" << std::endl;  
  		values.close();
	}
}

const Action* ProbPlanner::decideAction(const AtomSet& state_atoms,const ValueMap& state_values)
{
	std::ofstream file_experiments;
	bool success;

  	std::string previous_action = "";

  	// ------- Getting the static atoms ---------
  	if(index_current_action_==0)
	{
    	//Get the dynamic atoms
    	std::ostringstream os_dymanic_atoms;

    	for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++)
		{
      	os_dymanic_atoms << **ai;
    	}
    
		for (AtomSet::const_iterator ai = _problem.init_atoms().begin(); ai != _problem.init_atoms().end(); ai++) 
		{
	      std::ostringstream os_atom_aux;
	      os_atom_aux << **ai;
	      if(os_dymanic_atoms.str().find(os_atom_aux.str())==std::string::npos)
			{
				static_state_atoms_.insert(*ai);
      	}
    	}
  	}
  
  	// ------- Writting the observation of the Previous Action ---------
  
	if(index_current_action_ > 0)
  	{
		previous_action = plan_.at(index_current_action_-1);
    
    	// Success or Failure or Dead Deadend
    	
    	const ActionSet *allactions;
    	allactions = &_problem.actions();   
    	AtomSet predicted_state_atoms;
    	ValueMap predicted_state_values;
    
    	//Getting the predicted state

    	for (ActionSet::const_iterator ai = allactions->begin();ai != allactions->end(); ai++) 
		{     
      	std::ostringstream os_aux;
      	os_aux<<**ai;
      	if(strcasecmp(os_aux.str().c_str(), previous_action.c_str()) == 0)
			{
        		predicted_state_atoms = get_previous_state_atoms();
				predicted_state_values = get_previous_state_values();
				(*ai)->affect(_problem.terms(),predicted_state_atoms, predicted_state_values);
      	}
    	}    
    
    	// Comparing the predicted and the real state
    	success = true;
    	std::ostringstream os_real_state_aux;
    
		//Dynamic Atoms
    	for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++) 
		{	 	  
	      os_real_state_aux << **ai;
    	}
    
		//Static Atoms
    	for (AtomSet::const_iterator ai = static_state_atoms_.begin();ai != static_state_atoms_.end(); ai++) 
		{
	      os_real_state_aux << **ai;
	   }
	
	   std::ostringstream os_predicted_state_aux;
	   
		for (AtomSet::const_iterator ai = predicted_state_atoms.begin();ai != predicted_state_atoms.end(); ai++) 
		{	 	  
      	os_predicted_state_aux << **ai;
    	}    

    	for (AtomSet::const_iterator ai = state_atoms.begin();ai != state_atoms.end(); ai++) 
		{
			std::ostringstream os_real_atom_aux;	  
      	os_real_atom_aux << **ai;	  	  	  
      	if(os_predicted_state_aux.str().find(os_real_atom_aux.str())==std::string::npos) success=false;
    	}
    
    	for (AtomSet::const_iterator ai = predicted_state_atoms.begin();ai != predicted_state_atoms.end(); ai++) 
		{
	      std::ostringstream os_predicted_atom_aux;	  
	      os_predicted_atom_aux << **ai;	  
	      if(os_real_state_aux.str().find(os_predicted_atom_aux.str())==std::string::npos) success=false;
	   }

    	if(success == true) //Action executed
    	{
      		write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "success");
      		print_plan(previous_action);
      		success_actions++;
    	}
    	else
    	{ 
      		// No Plan Repair => Dead End     
      		if(repairplan()==false)
      		{      
				if(replan(state_atoms, state_values)==false)
				{
	  				std::cerr << PACKAGE ": No plan found for the new State: " << std::endl;
	  				std::cerr << PACKAGE ": Execution Dead End" << std::endl;
          		
					deadend_actions++;
	  				write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "deadend");      	
	  				return NULL;
				}
				else
				{
	  				//std::cout << "FAILURE" << std::endl;
	  				write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "failure");  
	  				failure_actions++;    
				}
      	}
			else
			{
				//std::cout << "FAILURE" << std::endl;
				write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "failure");	      	
				failure_actions++;
      	}      
    	}
  	}

  	if (index_current_action_ == plan_.size())
	{
    	std::cout<< "No more actions in the plan " << std::endl;
    	if (repairplan() == false)
		{
	      if (replan(state_atoms,state_values) == false)
				return NULL;
    	}
  	}

  	// Getting the action to execute        
  	ActionList actions;
  	_problem.enabled_actions(actions, state_atoms, state_values);

	if ((plan_[index_current_action_].find("_ABS") < plan_[index_current_action_].size()) || (plan_[index_current_action_].find("_abs") < plan_[index_current_action_].size()))
  	{
		std::cout << "ABSTRACT ACTION: " << plan_[index_current_action_] << std::endl;

		if (replan(state_atoms, state_values)==false)
		{
  			write_Observation(observations_file_name_, previous_action, state_atoms, state_values, "abstract");  
  			failure_actions++;   
		
			// Getting the action to execute        
  			ActionList actions;
  			_problem.enabled_actions(actions, state_atoms, state_values);
		}
  	}

  	std::string next_action = "";  
  	next_action 				= plan_.at(index_current_action_);
  	//Looking for the next action in the enabled_actions vector  
  
  	for (int i=0; i<actions.size(); i++)
  	{
		std::ostringstream os_aux;
    	os_aux<<*(actions[i]);   

    	if (strcasecmp(os_aux.str().c_str(), next_action.c_str())==0)
    	{
      	index_current_action_++;           
      	std::cout<< "Sent action " << index_current_action_ << ": " << os_aux.str()  << std::endl;
      	set_previous_state(state_atoms,state_values);   

      	return actions[i];      
    	}
  	}  

  	// Next action is wrong (there must be an error)

  	/*std::cout<< "The action " << next_action << " Can't be executed !!" << std::endl;*/  

	if (replan(state_atoms, state_values)==false)
	{
  		write_Observation(observations_file_name_, previous_action, get_previous_state_atoms(), get_previous_state_values(), "failure");	      	
		failure_actions++; 
		
		// Getting the action to execute        
  		ActionList actions;
  		_problem.enabled_actions(actions, state_atoms, state_values);
	}

  	next_action 				= plan_.at(index_current_action_);
  	//Looking for the next action in the enabled_actions vector  
  
  	for (int i=0; i<actions.size(); i++)
  	{
		std::ostringstream os_aux;
    	os_aux<<*(actions[i]);   

    	if (strcasecmp(os_aux.str().c_str(), next_action.c_str())==0)
    	{
      	index_current_action_++;           
      	std::cout<< "Sent action " << index_current_action_ << ": " << os_aux.str()  << std::endl;
      	set_previous_state(state_atoms,state_values);   

      	return actions[i];      
    	}
  	}

   /*file_experiments.open(experiments_file_name_.c_str(), std::ios::app);

   file_experiments << "Begin result experiment" << std::endl;
	file_experiments << "Problem name: " << str_arg_problem_name << std::endl;
        file_experiments << "Horizon: " << int_arg_horizon << std::endl;
        file_experiments << "Planinng time: " << planning_time << std::endl;
        file_experiments << "Execution: Error" << std::endl;
        file_experiments << "Success: " << success_actions << std::endl;
        file_experiments << "Failure: " << failure_actions << std::endl;
        file_experiments << "Deadend: " << deadend_actions << std::endl;
        file_experiments << "End result experiment" << std::endl << std::endl;
 
  	file_experiments.close();*/

  	return NULL;
}


int main(int argc, char **argv)
{
	time_t rawtime;	
	struct tm *timer;
	
	/* Set initial value time. */
 	float process_time 	= 0;
  	/* Set default verbosity. */
  	verbosity 		= 1;
  	/* Set default warning level. */
  	warning_level 		= 1;
  	
	if (argc < 8) 
	{
   	std::cerr << "Usage: " << *argv << " <host>:<port> <domain file> <abstract domain file> <domain name> <problem file> <problem name> <horizon> <planner> <heuristic>" << std::endl;
    	return 1;
  	}

  	if (!read_file(argv[2])) 
	{
    	std::cerr << "Couldn't read domain file " << argv[2] << std::endl;
    	return 1;
  	}
  	if (!read_file(argv[5])) 
	{
    	std::cerr << "Couldn't read problem file " << argv[4] << std::endl;
    	return 1;
  	}

  	const Problem *problem = Problem::find(argv[6]);

  	if (!problem) 
  	{
    	std::cerr << "Problem " << argv[6] << " is not defined in " << argv[5] << std::endl;
    	return 1;
  	}

  	//Capturing the arguments
  	str_arg_domain_file_name	= argv[2];
 	str_arg_abstract_file_name	= argv[3];
  	str_arg_domain_name		= argv[4];
  	str_arg_problem_file_name	= argv[5];
  	str_arg_problem_name		= argv[6];
  	int_arg_horizon			= atoi(argv[7]);
	int_arg_planner			= atoi(argv[8]);
	
std::cout << "Paso1 " << argc << argv[9] << std::endl;
	
	if (argc > 9)	
		int_arg_heuristic	= atoi(argv[9]);
	else
		int_arg_heuristic 	= 1;

	if (argc > 10)
	{

std::cout << "Paso1 " << argc << argv[10] << std::endl;
		int_arg_noabs 		= atoi(argv[10]);
		std::cout << "NO ABSTRACTIONS " << argv[10] << std::endl;
	}

std::cout << "Paso1" << std::endl;

	time( &rawtime );
	timer = gmtime( &rawtime );

std::cout << "Paso1" << std::endl;

	exp_file_name << "../experiment_" << timer->tm_hour << "_" << timer->tm_min << "_" << timer->tm_mday << "_" << (timer->tm_mon + 1) << "_" << (timer->tm_year + 1900);

   	obs_file_name << "../observation_" << str_arg_problem_name << "_" << int_arg_horizon << "_" << timer->tm_mday << "_" << (timer->tm_mon + 1) << "_" << (timer->tm_year + 1900);
 
std::cout << "Paso1" << std::endl;

  	char *hostport 			= argv[1];
  	char *host 			= strtok(hostport, ":");
  	char *portstr 			= strtok(0, ":");
	int port 			= atoi(portstr);

  	try
	{
    
    	int socket = connect(host, port);
    	if (socket <= 0) 
	{
     		std::cerr << "Could not connect to " << host << ':' << port << std::endl;
		return 1;
	}  

		std::cout << "Paso1" << std::endl;

    	ProbPlanner pplanner(argv[2], argv[3], obs_file_name.str(), exp_file_name.str(), *problem);    

		std::cout << "Paso2" << std::endl;

    	XMLClient(pplanner, *problem, "result", socket);
    
  	} 
	catch (const std::exception& e) 
	{
   		std::cerr << std::endl << "mdpclient: " << e.what() << std::endl;
    		return 1;
  	} 
	catch (...) 
	{
   		std::cerr << "mdpclient: fatal error" << std::endl;
    		return -1;
  	}
  
  	Problem::clear();
  	Domain::clear();
  
  	return 0;
}
